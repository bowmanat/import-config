#!/bin/bash

if [ $# -eq 0 ] || ! [ -f "$1" ]; then
cat <<- END_TEXT
$0 {names_file}

{names_file} contains a list of the files you would like to keep here with one
file per line. The list of filenames should contain the absolute paths.
If you want an entire directory, just add the directory
to {names_file} and do not add the individual files.
END_TEXT
    exit 1
fi

retval=0

link_all() {
    for i in $@ ; do
        if ! [ -e "$i" ]; then
            echo "$i does not exist; skipping"
            retval=1
        elif [ -d "$i" ] ; then
            link_all `find "$i" -type f ! -path "$i" | xargs realpath -s`
        elif [ -e ./"$i" ] ; then
            echo ./"$i is already here; skipping"
            retval=1
        else
            mkdir -p ./`dirname "$i"`
            
            mv "$i" ./"$i" && \
                ln -s `realpath -s ./"$i"` "$i" && \
                echo "Moved and linked $i"

        fi
    done
}


link_all $(cat "$1")

exit ${retval}
