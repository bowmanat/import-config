#!/bin/bash

EXCLUDE=("." ".gitignore" "full-export.sh" "export-settings.sh"
         "import-settings.sh" "tester.sh" "tst_inputfiles.txt"
         "tst_outputfiles.txt" "tst_extrafiles.txt" "*.swp" "TODO.txt")
EXCLUDE_DIR=(".git")

# Instead of getting everything in working directory,
# get everything listed in file pointed to by argument
if ! [ -z "$1" ] ; then
    if ! [ -e "$1" ] ; then
        echo "$1 does not exist."
        exit 1
    else
        for i in $(cat "$1") ; do
            ./export-settings.sh "$i"
        done
    fi

# With no arguments, get everything in working directory that is not
# explicitly excluded
else
    find . $(printf " ! ( -path ./%s -prune ) " ${EXCLUDE_DIR[*]}) \
        $(printf " ! -name %s " ${EXCLUDE[*]}) \
        -type f \
        -exec ./export-settings.sh '{}' \;
fi
