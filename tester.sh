#!/bin/bash


inprefix="/home/nate/"
outprefix="./home/nate/"
fnames=("fake1" "fake2" "fakedir/f1" "fakedir/f2" "2fakedir/f3" "2fakedir/f4")
dirnames=("fakedir" "2fakedir")
input_name="tst_inputfiles.txt"
output_name="tst_outputfiles.txt"
extra_name="tst_extrafiles.txt"


test_exist() {
    # Tests whether the first list of files exists and the second does not

    if [[ $# -ne 3 ]]; then
        echo 'test_exist called with wrong number of arguments'
        exit 1
    fi

    local should_exist=("${!1}")
    local not_exist=("${!2}")
    local prefix="$3"
    local something_wrong=false

    for ex in ${should_exist[*]}; do
        if ! [[ -f ${prefix}${ex} ]] ; then
            something_wrong=true
            echo "EE: ${prefix}${ex} does not exist"
        fi
    done
    for nx in ${not_exist[*]}; do
        if [[ -f ${prefix}${nx} ]] ; then
            something_wrong=true
            echo "EE: ${prefix}${nx} exists"
        fi
    done

    if [ ${something_wrong} = true ]; then
        echo -e '\n\nSomething went wrong\n\n'
    else
        echo -e '\n\nEverything went well\n\n'
    fi
}


clean_up() {
    if [[ $# -lt 1 ]]; then
        echo 'Cleanup called without prefix'
        exit 1
    fi

    local prefixes=("$@")

    for prefix in ${prefixes[*]}; do
        for i in ${dirnames[*]}; do
            rm -f "${prefix}$i"
        done
        for i in ${fnames[*]}; do
            rm -f "${prefix}$i"
        done
        for i in ${dirnames[*]}; do
            rmdir "${prefix}$i"
        done
    done
}


# Clean up before
clean_up ${inprefix} ${outprefix} 2> /dev/null

# Create files to "backup"
for dn in ${dirnames[*]}; do
    mkdir "${inprefix}${dn}"
done
for fn in ${fnames[*]}; do
    touch "${inprefix}${fn}"
done

# Create files containing lists of files
cat > ${input_name} <<- eof
${inprefix}fake1
${inprefix}fake2
${inprefix}fakedir
${inprefix}doesnotexist
${inprefix}2fakedir/f3
${inprefix}fake1
eof

cat > ${output_name} <<- eof
${inprefix}fake1
${inprefix}fake2
${inprefix}fakedir
${inprefix}doesnotexist
${inprefix}2fakedir/f3
${inprefix}fake1
eof

cat > ${extra_name} <<- eof
${inprefix}2fakedir
eof



# Input tests:
# ------------
# Link regular files
# Link entire directory
# Ignore nonexistant files
# Link one file from directory (must create directory)
# Check for files already in repo -> should ignore
# Link entire directory after having some of its files


# Test whether list of input files exists -> should print message
echo -e '\n\n\n-----Testing without input-----\n'
./import-settings.sh

# Import some of the files
echo -e '\n\n\n-----Testing with input-----\n'
./import-settings.sh ${input_name}

should=("fake1" "fake2" "fakedir/f1" "fakedir/f2" "2fakedir/f3")
shouldnt=("2fakedir/f4" "doesnotexist")
test_exist should[@] shouldnt[@] "${outprefix}"
            
# Import the rest of the files
echo -e '\n\n\n-----Testing with more input-----\n'
./import-settings.sh ${extra_name}

should=${fnames}
shouldnt=("doesnotexist")
test_exist should[@] shouldnt[@] "${outprefix}"


# Output tests:
# -------------
# Link regular files
# Link entire directory
# Ignore nonexistant files
# Link one file from directory
# Check for files already in filesystem -> should ignore
# Check for directories already in filesystem -> should ignore
#   In the future, might want to go into
#   already existing directories and link relevant files


# Do nothing if files are already there
echo -e '\n\n\n-----Should not overwrite files-----\n'
./full-export.sh

# Clear the files
clean_up ${inprefix} 2> /dev/null

# Run full export -> should export everything
echo -e '\n\n\n-----Should write all files-----\n'
./full-export.sh

should=${fnames}
shouldnt=("doesnotexist")
test_exist should[@] shouldnt[@] "${inprefix}"

# Remove exported files to prepare for next test
clean_up ${inprefix} 2> /dev/null

# Run with outputfile
echo -e '\n\n\n-----Write files from list-----\n'
./full-export.sh "${output_name}"

should=("fake1" "fake2" "fakedir/f1" "fakedir/f2" "2fakedir/f3")
shouldnt=("2fakedir/f4" "doesnotexist")
test_exist should[@] shouldnt[@] "${inprefix}"

# Run with extras
echo -e '\n\n\n-----Write remaining files from list-----\n'
./full-export.sh "${extra_name}"

should=${fnames}
shouldnt=("doesnotexist")
test_exist should[@] shouldnt[@] "${inprefix}"

# Clean up after
clean_up ${inprefix} 2> /dev/null
rm -r `dirname ${outprefix}`
rm -f ${input_name} ${output_name} ${extra_name}
