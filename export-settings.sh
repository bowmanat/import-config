#!/bin/bash

if [ $# -eq 0 ] ; then
    echo 'Please pass in a filename'
    exit 1
fi

retval=0

put_all() {
    for i in $@; do

        # Remove leading . , which can come from using find to pass input
        # Changes relative path into corresponding absolute path
        fname=`echo $i | sed "s|^\.||"`

        if ! [ -e ./"${fname}" ] ; then
            echo ./"${fname} does not exist"
            retval=1
        elif [ -d ./"${fname}" ] ; then
            put_all `find ./"${fname}" -type f ! -path ./"${fname}"`
        elif [ -e "${fname}" ] ; then
            echo "${fname} already exists; ignoring"
            retval=1
        else
            mkdir -p `dirname ${fname}`
            ln -s `realpath -s ./"${fname}"` "${fname}"
        fi
    done
}


put_all "$@"

exit ${retval}
